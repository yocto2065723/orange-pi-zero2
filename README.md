# orange-pi-zero2

## Description

This repo provides yocto image for Orange Pi Zero 2 basad on Allwinner H616 CPU
To build image do the next steps.

## Build image

```
git clone https://gitlab.com/yocto2065723/orange-pi-zero2.git
git checkout develop
bitbake core-image-minimal
```
Go to build/tmp/deploy/images/orange-pi-zero2

```
sudo dd if=core-image-minimal-orange-pi-zero2-20240606153931.rootfs.sunxi-sdimg of=/dev/sda bs=4M status=progress
```

## Build From Zero state
In details you can check the all build process.
It is shown below

```
git clone git://git.yoctoproject.org/poky
cd poky
git checkout -t origin/kirkstone -b my-kirkstone
cd ..
git clone https://github.com/linux-sunxi/meta-sunxi
git clone git://git.yoctoproject.org/meta-arm
git clone git://git.openembedded.org/meta-openembedded
cd meta-sunxi
git checkout kirkstone
cd .. && cd meta-arm
git checkout kirkstone
cd .. && cd meta-openembedded
git checkout kirkstone

source ./oe-init-build-env
bitbake-layers add-layer ../../meta-openembedded/meta-oe
bitbake-layers add-layer ../../meta-openembedded/meta-python
bitbake-layers add-layer ../../meta-sunxi
```
Change MACHINE in build/conf/local.conf to MACHINE ??= "orange-pi-zero2"

```
bitbake core-image-minimal
```

Go to build/tmp/deploy/images/orange-pi-zero2

```
sudo dd if=core-image-minimal-orange-pi-zero2-20240606153931.rootfs.sunxi-sdimg of=/dev/sda bs=4M status=progress
```


